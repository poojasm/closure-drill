function limitFunctionCallCount(cb, n) {
  try {
    let count = 1;
    return function () {
      if (count <= n) {
        count++;
        let results = cb();
        return results;
      } else {
        return null;
      }
    };
  } catch (err) {
    console.log(err.name);
  }
}

module.exports = limitFunctionCallCount;
