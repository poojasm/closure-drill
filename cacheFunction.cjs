function cacheFunction(cb) {
  try {
    const cache = {};

    return function (...args) {
      const key = JSON.stringify(...args);
      if (cache.hasOwnProperty(key)) {
        return cache[key];
      } else {
        const result = cb(...args);
        cache[key] = result;
        return result;
      }
    };
  } catch (err) {
    console.log(err.name);
  }
}

module.exports = cacheFunction;
