const limitFunctionCallCount = require("../limitFunctionCallCount.cjs");

const callBack = function () {
  return "hello";
};
const result = limitFunctionCallCount(callBack, 4);

console.log(result());
console.log(result());
console.log(result());
console.log(result());
console.log(result());
console.log(result());
