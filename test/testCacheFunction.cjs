const cacheFunction = require("../cacheFunction.cjs");

const callBack = function (x, y) {
  return x * y;
};
const result = cacheFunction(callBack);

console.log(result(10, 4));
