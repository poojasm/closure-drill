function counterFactory() {
  try {
    let counter = 0;
    const operation = {
      increment: function () {
        ++counter;
        return counter;
      },
      decrement: function () {
        --counter;
        return counter;
      },
    };
    return operation;
  } catch (err) {
    console.log(err.name);
  }
}

module.exports = counterFactory;
